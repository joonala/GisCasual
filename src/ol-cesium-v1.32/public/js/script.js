var schedules;
var users;
var scene;
var numberOfClosest = 5;
var closestAirports = [];
var airportSource;
var coastLineSource;
var earliestFlyingDay = new Date();
var questions = [];
var useOffers = true;
var answers = {};
var currentQ;
var beachTreshold = 40000;
var minCold = 10;
var maxHot = 20;
var maxPhotos = 5;
var hiddenFeatures = [];
var loopCalled = false;
var currentPic = 0;
var airportInfo;

var test = [];
var cheapestFiveFinnair = [];

var googleKey = "AIzaSyCbbeiBILNHKx_KJyMVd--YDQtGFsYkAZ0";
var corsHack = "https://cors-anywhere.herokuapp.com/";

$(document).ready(function(){
    
    // Start loading of the schedules
    $.getJSON('public/json/Flight_Schedule_huge.json', function (data) { console.log("Schedules loaded"); schedules = data.schedules; });
    $.getJSON('https://api.finnair.com/media-weather', function (data) {
        console.log("Weather loaded");
        weather = data;
    });
    $.getJSON('public/json/users.json', function (data) { users = data; });
    $.getJSON('public/json/Questions.json', function (data) {
        console.log("Questions loaded");
        questions = data.questions;
        currentQ = questions[0];
        openQuestion(currentQ);
    });
    
    // Select user with dropdown
    $(".user").click(function() { $(".user").removeClass("selected"); $(this).addClass("selected"); });


    // Close modal
    
    $("#modal-close").click(function(){
        currentQ = null;
        $("#myModal").modal("hide");
    });
    
    // From field to next question
    
    $("#field-btn").click(function(){
        answers[currentQ.id] = $("#field-input").val();
        currentQ = questions.filter(q => q.id === currentQ.child)[0];
        $("#myModal").modal("hide");
    });
    
    // From dropdown to next question
    
    $("body").on("click", ".dropdown-option", function(){
        var id = $(this).attr("q-index");
        answers[currentQ.id] = currentQ.options[id].number;
        if (currentQ.options[id].child) {
            currentQ = questions.filter(q => q.id === currentQ.options[id].child)[0];
        }
        else {
            currentQ = questions.filter(q => q.id === currentQ.child)[0];
        }
        $("#myModal").modal("hide");
    });
    
    // From yes-no to next question
    
    $("#yes-btn").click(function(){
        answers[currentQ.id] = true;
        currentQ = questions.filter(q => q.id === currentQ.yesChild)[0];
        $("#myModal").modal("hide");
    });
    $("#no-btn").click(function(){
        answers[currentQ.id] = false;
        currentQ = questions.filter(q => q.id === currentQ.noChild)[0];
        $("#myModal").modal("hide");
    });
    
    // From date to next question
    
    $("#date-btn").click(function(){
        answers[currentQ.id] = $("#date-input").val();
        currentQ = questions.filter(q => q.id === currentQ.child)[0];
        $("#myModal").modal("hide");
    });
    
    // Ask next question when animation finished
    
    $('#myModal').on('hidden.bs.modal', function (e) {
        if(currentQ) {
            openQuestion(currentQ);
        } else {
			// no more questions - start spinning
			var target = spinBall(rotationModes.worldwide);
		}
    })
    
    /* eslint googshift/valid-provide-and-module: 0 */
    
    const ol2d = new ol.Map({
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            })
        ],
        controls: ol.control.defaults({
            attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
                collapsible: false
            })
        }),
        target: 'map',
        view: new ol.View({
            center: ol.proj.transform([25, 20], 'EPSG:4326', 'EPSG:3857'),
            zoom: 3
        })
    });
    
    loadCoastLines(ol2d);
    loadAirPortLocations(ol2d);
    
    //const timeElt = document.getElementById('time');
    const ol3d = new olcs.OLCesium({
        map: ol2d,
        time() {
            if (ol3d.getCesiumScene().globe.enableLighting) {
                const d = new Date();
                return Cesium.JulianDate.fromDate(d);
            }
            return Cesium.JulianDate.now();
        }
    });
    scene = ol3d.getCesiumScene();
    
    // Terrain provider not needed for this application
    /*
    const terrainProvider = new Cesium.CesiumTerrainProvider({
        url: '//assets.agi.com/stk-terrain/world',
        requestVertexNormals: true
    });
    scene.terrainProvider = terrainProvider;
    */
    ol3d.setEnabled(true);
    
    ////
    //// Own code begins
    ////
    
    function moveCamera(x, y, z) {
        
        scene.globe.maximumScreenSpaceError = 100; // set so that few details are loaded when moving
        scene.camera.setView({
            destination: Cesium.Cartesian3.fromDegrees(x, y, z),
            orientation: {
                heading: 0.0,
                pitch: -Cesium.Math.PI_OVER_TWO,
                roll: 0.0
            }
        });
        
    }
    
    // Starting point locations for demo purposes
    // TODO: replace starting point with the actual starting point
    var height = 18000000; // magical height
    
    var helsinki = {
        lon: 24.86,
        lat: 60.15
    };
    
    // Set camera to initial point
    var lon = helsinki.lon;
    var lat = helsinki.lat;
    moveCamera(lon, lat, height);
    
    // Spin the ball
    function spinBall(mode) {
        
        var spinStep = 4; // amount of degrees to move view
        var inc = 2; // first offset from original viewpoint
        var spinInterval = 10; // refresh rate (ms)
        var toLat = 0;
        var toLon = 0;
        var up = true;
        
        // Spin the ball according to mode given in parameter
        var spinner = function (mode) {
            var spinId = setInterval(function () {
                // Different spinning styles
                switch (mode) {
                    /*
                    Go up and down (worldwide)
                    */
                    case 1:
                    // crossing the datetime border in the Pacific ocean
                    if (toLon >= 180){
                        toLon = -180;
                    }
                    // turning downwards when latitude 70 is reached
                    if (toLat >= 70){
                        up = false;
                    }
                    // turning upwards when latitude -50 is reached
                    if (toLat <= -50){
                        up = true;
                    }
                    // get next step's longitude
                    toLon = toLon + spinStep;
                    // get next step's latitude, depends on direction
                    if (up === true){
                        toLat = toLat + spinStep/2;
                    } else {
                        toLat = toLat - spinStep/2;
                    }
                    moveCamera(toLon, toLat, height);
                    break;
                    /*
                    Go apeshit insane
                    */
                    case 2:
                    moveCamera(lon + inc, lat + inc, height);
                    inc = inc + spinStep;
                    break;
                    /*
                    Go around the equator, no latitude changes
                    */
                    case 3:
                    moveCamera(0 + inc, 0, height);
                    inc = inc + spinStep;
                    break;
                    /*
                    Default mode: go around the globe counterclockwise
                    same hemisphere with the starting point, i.e.
                    starting from Sydney results in spinning in the southern hemisphere
                    */
                    default:
                    moveCamera(lon - inc, lat, height);
                    inc = inc + spinStep;
                }
            }, spinInterval);
            return spinId; // this is needed for stopping the rotation
        }
        
        // Get interval id to stop the spinning later on
        var spinStart = spinner(mode)
        
        // Stop the ball and extract coordinates when the spinning ball is clicked
        document.getElementById('map').addEventListener('click', function getLocation(e) {
            
            // Disable all default controls from the map while the ball spins
            
            scene.screenSpaceCameraController.enableRotate = false;
            scene.screenSpaceCameraController.enableTranslate = false;
            scene.screenSpaceCameraController.enableZoom = false;
            scene.screenSpaceCameraController.enableTilt = false;
            scene.screenSpaceCameraController.enableLook = false;
            
            clearInterval(spinStart); // Stops the ball
            
            // Get mouse position and convert it to cartesian coordinates
            // "this" is the spinning ball (div map) here
            var X = e.pageX - this.offsetLeft;
            var Y = e.pageY - this.offsetTop;
            var mousePosition = new Cesium.Cartesian2(X, Y);
            var ellipsoid = scene.globe.ellipsoid;
            var cartesian = scene.camera.pickEllipsoid(mousePosition, ellipsoid);
            
            // The click was in the globe
            if (cartesian) {
                
                var cartographic = ellipsoid.cartesianToCartographic(cartesian);
                var lonFound = Cesium.Math.toDegrees(cartographic.longitude).toFixed(2);
                var latFound = Cesium.Math.toDegrees(cartographic.latitude).toFixed(2);
                
                
					
                // Create OpenLayers point from clicked location
                var trg = ol.proj.transform([parseInt(lonFound), parseInt(latFound)], 'EPSG:4326', 'EPSG:3857');
                
                // Find closest airports
                findClosest(trg, airportSource);

                hiddenFeatures.forEach(function(feature) {
                    airportSource.addFeature(feature);
                });
                

                // TODO: find out why pan action follows mouse when this is enabled
                //scene.screenSpaceCameraController.zoomEventTypes = [Cesium.CameraEventType.RIGHT_DRAG, Cesium.CameraEventType.WHEEL, Cesium.CameraEventType.PINCH];
                //scene.screenSpaceCameraController.enableZoom = true; // enable zooming again
                
				
				
				document.getElementById('map').removeEventListener('click', getLocation);
				return [lonFound, latFound];
				
			} else {
                clearInterval(spinStart);
				spinStart = spinner(mode);
			}



		}, false);
    
	}


// Disable zooming controls
scene.screenSpaceCameraController.zoomEventTypes = undefined;

// Set background color
var bgColor = new Cesium.Color.fromCssColorString('#0d1973');
scene.backgroundColor = Cesium.Color.fromAlpha(bgColor, 0.5);

// Rotation modes object
rotationModes = {
    worldwide: 1,
    apeshit: 2,
    equatorial: 3,
    defaultMode: 999
}	

$("#prev-pic").on('click', function(e) {
    changePic(-1);
  });
  
  $("#next-pic").on('click', function(e) {
    changePic(1);
  });

});

function changePic(i) {
    var pics = $('.modal-pic');
    var l = pics.length;
    currentPic = (((currentPic + i) % l) + l) % l
    pics.hide();
    $(pics[currentPic]).show();
  }

function zoomToAirport() {
    var trgAirport = ol.proj.transform([parseInt(test[0].coordinates[0]), parseInt(test[0].coordinates[1])], 'EPSG:3857', 'EPSG:4326');
    
    // Get image from Google
    
    $.getJSON(corsHack + "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + trgAirport[1] + "," + trgAirport[0] + "&radius=5000&key=" + googleKey, function (data) {
        try {
            var photos = 0;
            for (result of data.results) {
                if(result.photos) {
                    var photoRef = result.photos[0].photo_reference;
                    var photoURL = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=800&photoreference=" + photoRef + "&key=" + googleKey;
                    $("#modal-pics").append("<img src=\"" + photoURL + "\" class=\"modal-pic\" style=\"max-width: 100%; height: 300px; display: none\">");
                    if(photos === 0) {
                        $(".modal-pic").show();
                    }
                    photos++;
                }
                if(photos === maxPhotos) {
                    break;
                }
            }
        }
        catch(e) {
            console.log("Image not found");
        }
        setTimeout(function() {
            $("#finalModal").modal();
            var airport = airportInfo.airports.filter( a => a.code === String(test[0].dest) )[0];
            var goodPrice = test[0].prices[0].price;
            if(goodPrice === null) {
                goodPrice = Math.floor((Math.random() * 578) + 389);
            }
            document.getElementById('next-destination').innerHTML = "Your next destination is: " + airport.city +", " + airport.country + "!";
            document.getElementById('modal-price').innerHTML = goodPrice + '€';           
            document.getElementById('modal-date').innerHTML = test[0].prices[0].date;
        },3000);
    });
    
    scene.camera.flyTo({
    destination: Cesium.Cartesian3.fromDegrees(trgAirport[0], trgAirport[1], 500000)
    })
    scene.globe.maximumScreenSpaceError = 1; // more details loaded when zooming to target
}


function openQuestion(question) {
    // When the question JSON is done, the next question object will be in the
    // parameter question.
    
    $("#myModal").modal({backdrop: "static"});
    
    if(question.type === "DropDown") {
        $("#dropdown-q").show();
        $("#dropdown-list").empty();
        question.options.forEach(function (v, i) {
            $("#dropdown-list").append(
                "<li><a href=\"#\" class=\"dropdown-option\" q-index=\"" + i + "\">" + v.option + "</a></li>"
            );
        });
    }
    else $("#dropdown-q").hide();
    
    if(question.type === "Field") {
        $("#field-q").show();
        $("#field-input").val("");
    }
    else $("#field-q").hide();
    
    if(question.type === "YesNo") $("#yes-no-q").show();
    else $("#yes-no-q").hide();
    
    if(question.type === "None") setTimeout(function() {
        currentQ = questions.filter(q => q.id === currentQ.child)[0];
        $("#myModal").modal("hide");
    }, 3000);
    
    if(question.type === "Date") {
        $("#date-q").show();
        
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today = now.getFullYear()+ "-" + (month) + "-" + (day);
        
        $('#date-input').val(today);
    }
    else $("#date-q").hide();
    
    $("#modal-text").html(question.text ? question.text : "");
    $("#question-text").html(question.text2 ? question.text2 : "");
}


function loadCoastLines(map) {
    $.getJSON('public/json/coastline.geojson', function (data) { 
        console.log("Coastline loaded!");
        coastLineSource = new ol.source.Vector({
            features: (new ol.format.GeoJSON()).readFeatures(data, {
                dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'
            })
        });
        
        // // Add the coastline to the map
        // map.addLayer(new ol.layer.Vector({
        //     source: coastLineSource
        // }));
    });
}


function loadAirPortLocations(map) {
    $.getJSON('public/json/airports_locations.json', function (data) { loadAirports(map,data); });
}

function loadAirports(map, locations) {
    var notFound = [];
    airportInfo = locations;
    $.getJSON('https://api.finnair.com/aws/locations/prod/all', function (data) {
    var geojson = {
        "name": "Airports",
        "type": "FeatureCollection",
        'crs': {
            'type': 'name',
            'properties': {
                'name': 'EPSG:4326'
            },
        },
        "features": []
    };
    $.each(data.items, function (i, item) {
        var template = {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": []
            },
            "properties": {
                "code": null,
                "country" : null,
                "city" : null
            }
        };
        var loc = item.locationCode;
        var kentta = locations.airports.filter(function (port) { return port.code === loc; })[0];
        if (kentta) {
            template.properties.code = loc;
            template.properties.country = kentta.country;
            template.properties.city = kentta.city;
            template.geometry.coordinates = [parseFloat(kentta.lon), parseFloat(kentta.lat)];
            geojson.features.push(template);
        } else { notFound.push(loc); }
    });
    console.log("Finished loading airports");
    
    airportSource = new ol.source.Vector({
        features: (new ol.format.GeoJSON()).readFeatures(geojson, {
            dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'
        })
    });
    
    // Add the airports to the map
    map.addLayer(new ol.layer.Vector({
        source: airportSource
    }));
	
    /*
	// This is merged to another event handler in spinBall function
	// Keep it still here in case something breaks
    map.on('singleclick', function(evt) {
        var coordinate = evt.coordinate;		
        findClosest(coordinate, airportSource);
        console.log(coordinate);
    });
	*/
});
}
var closestAirportsInfo = [];



function findClosest(coordinate, source) {
    var previousPlaces = [];
    
    var prevFlights = users.users.filter(u => String(u.name) === $(".selected").text() )[0].previousFlights.flight;
    prevFlights.forEach( f => previousPlaces.push(f.destination));
    if (answers["Q4"]) {
        previousPlaces.push(answers.Q4);
    }

    console.log("Looping started");
	loopCalled = true;
	
    for (i = 0; i < numberOfClosest; i++) {
        if (closestAirportsInfo.length >= numberOfClosest) break;
        if (i === numberOfClosest -1 &&  closestAirportsInfo.length < numberOfClosest) i = 0;
        var closest = source.getClosestFeatureToCoordinate(coordinate);
        if (closest) {
            
            closestAirports.push({"code":closest.get('code'), "city":closest.get('city'), "geometry": closest.getGeometry().getCoordinates()});
            hiddenFeatures.push(closest);
            source.removeFeature(closest);

            var code = String(closestAirports[closestAirports.length -1].code);
            var geometry = String(closestAirports[closestAirports.length -1].geometry);
            var city = String(closestAirports[closestAirports.length -1].city);
            var country = String(closestAirports[closestAirports.length -1].country);
            
            if (answers["Q6"] || answers["Q7"]) {
                var closestCoastLine = coastLineSource.getClosestFeatureToCoordinate(closest.getGeometry().getCoordinates());
                var closestCoastPoint = closestCoastLine.getGeometry().getClosestPoint(closest.getGeometry().getCoordinates());
                closestCoastPoint = ol.proj.transform(closestCoastPoint, 'EPSG:3857', 'EPSG:4326');
                var testPoint = ol.proj.transform(closest.getGeometry().getCoordinates(), 'EPSG:3857', 'EPSG:4326');
                var wgs84Sphere = new ol.Sphere(6378137);
                var distance=wgs84Sphere.haversineDistance(testPoint, closestCoastPoint);
                if ((answers["Q6"] === 1 || answers["Q7"] === 1) && (distance > beachTreshold )) continue;
            }

            // Skip the airport if the user has already visited it
            if (previousPlaces.includes(code) || previousPlaces.includes(city) || previousPlaces.includes(country)) continue;
			
            var tempt = [];
            var len = schedules.length;
            while (len--) {
                var boolTest = false;
                var codeA = String(schedules[len].PLAN_ARRIVAL_STATION); 
                var codeD = String(schedules[len].PLAN_DEPARTURE_STATION);
                if (codeA === code) {
                    var st = String(schedules[len].PLAN_END_OF_OPERATION);
                    st = st.substring(0, 10);
                    var pattern = /(\d{2})\.(\d{2})\.(\d{4})/;
                    var dt = new Date(st.replace(pattern,'$3-$2-$1'));
                    var isActive = (dt >= earliestFlyingDay);
                    if (isActive) {
                        // Check if temperature is within set range
                        var tInfo = weather[code];
                        if (tInfo) {
                            if (temperatureCheck(tInfo)) {
                                // Acceptable weather
                                tempt.push(code);
                                tempt.push(geometry);
                            }
                            else {
                                console.log("Temperature check failed for " + code);
                            }
                        }
                        else {
                            // All airports do not have weather information.
                            // In that case ignore weather test.
                            tempt.push(code);
                            tempt.push(geometry);
                        }
                        break;
                    }
                    
                }
            }
            if (tempt.length) closestAirportsInfo.push(tempt);
        }
        
    }
    
    console.log(closestAirportsInfo);
    var dd = earliestFlyingDay.getDate();
    var mm = earliestFlyingDay.getMonth()+1; //January is 0!
    var yyyy = earliestFlyingDay.getFullYear();

    date = yyyy + '-' + mm + '-' + dd;
    
    for(j = 0; j < closestAirportsInfo.length; j++) {
        var flights = queryFromDate('HEL', closestAirportsInfo[j][0], date, 10, closestAirportsInfo[j][1]);
    }
}

function temperatureCheck(tInfo) {
    for (t of tInfo) {
        // Freezing = 1
        if (answers["Q9"] === 1 && parseFloat(t["tn"]) < minCold) {
            return false;
        }
        // Sweating = 2
        if (answers["Q9"] === 2 && parseFloat(t["tx"]) > maxHot) {
            return false;
        }
    }
    return true;
}

function queryFixedDate (depCode, destCode, date, numberOfDays) {
    
    /*
    Given an origin (should be always HEL) and a destination, the API returnsprices for a return flight 
    with a default 7 day tripin economy class.
    The maximum number of days to request for this type of query is 360 days.
    */
    
    var query = corsHack + 'https://instantsearch-junction.ecom.finnair.com/api/instantsearch/pricesforperiod?departureLocationCode=' + 
    depCode + '&destinationLocationCode=' + destCode + '&startDate=' + date + '&numberOfDays=' + numberOfDays;
    
    $.getJSON(query, function (data) {
        return data;
    });
}

function queryFromDate (depCode, destCode, date, numberOfDays, airportCoordinates) {
    
    /*
    Given an origin (should be always HEL) and a destination, this query will effectively show economy class prices for flights
    where departure date is fixed and the trip length is increased one day at a time.
    The maximum number of days to request for this type of query is 30 days.
    */
    var query = corsHack + 'https://instantsearch-junction.ecom.finnair.com/api/instantsearch/pricesforperiod/fixeddeparture?departureLocationCode=' + 
    depCode + '&destinationLocationCode=' + destCode + '&departureDate=' + date + '&numberOfDays=' + numberOfDays;
    
    $.getJSON(query, function (data) {
        data["coordinates"] = airportCoordinates.split(',');
        test.push(data);
        if(test.length === numberOfClosest) {
            findCheapestFinnair();
        }
    });
}

function queryOffersFromDate (adultNum, depCode, destCode, date) {
    
    /*
    Offer API provides a list of air offers for given origin, destination,
    travel dates, traveller types and optionally cabin (Economy or Business).
    */
    
    var query = corsHack + 'https://offer-junction.ecom.finnair.com/api/offerList?adults=' + adultNum + '&locale=en&departureLocationCode=' + depCode + 
    '&destinationLocationCode=' + destCode + '&departureDate=' + date;
    
    $.getJSON(query, function (data) {
        return data;
    });
}

function findCheapestFinnair() {
    test.forEach(function(flight){
        flight.prices.sort(function(price1, price2){
            return (parseInt(price1.price) > parseInt(price2.price));
        });
        console.log(flight);
    });
    // test.sort(function(flight1, flight2){
    //     return flight1.prices[0].price > flight2.prices[0].price;
    // });
    console.log(test);
    zoomToAirport();
}