/* eslint googshift/valid-provide-and-module: 0 */
const ol2d = new ol.Map({
  layers: [
    new ol.layer.Tile({
      source: new ol.source.OSM()
    })
  ],
  controls: ol.control.defaults({
    attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
      collapsible: false
    })
  }),
  target: 'map',
  view: new ol.View({
    center: ol.proj.transform([25, 20], 'EPSG:4326', 'EPSG:3857'),
    zoom: 3
  })
});

const timeElt = document.getElementById('time');
const ol3d = new olcs.OLCesium({
  map: ol2d,
  time() {
    const val = timeElt.value;
    if (ol3d.getCesiumScene().globe.enableLighting && val) {
      const d = new Date();
      d.setUTCHours(val);
      return Cesium.JulianDate.fromDate(d);
    }
    return Cesium.JulianDate.now();
  }
});
const scene = ol3d.getCesiumScene();
const terrainProvider = new Cesium.CesiumTerrainProvider({
  url: '//assets.agi.com/stk-terrain/world',
  requestVertexNormals: true
});
scene.terrainProvider = terrainProvider;
ol3d.setEnabled(true);

timeElt.style.display = 'none';
const toggleTime = function() { // eslint-disable-line no-unused-vars
  scene.globe.enableLighting = !scene.globe.enableLighting;
  if (timeElt.style.display == 'none') {
    timeElt.style.display = 'inline-block';
  } else {
    timeElt.style.display = 'none';
  }
};

////
//// Own code begins
////

function moveCamera(x,y,z){
  scene.camera.setView({
    destination: Cesium.Cartesian3.fromDegrees(x,y,z),
    orientation: {
      heading: 0.0,
      pitch: -Cesium.Math.PI_OVER_TWO,
      roll: 0.0
    }
  });
  console.log('camera set');
}

// Starting point location
var height = 20000000; // magical height
var lon = 25;
var lat = 60;

// Set camera to initial point (Helsinki?)
moveCamera(lon,lat,height);

// Spin the ball
function spinBall(){
	
	var inc = 1; // first offset from original viewpoint
	var spinStep = 1; // amount of degrees to move view
	var spinInterval = 100; // refresh rate
	
	var spinStart = setInterval(function(){
		moveCamera(lon+inc,lat,height/2);
		inc = inc + spinStep;
	},100);
	
	// Stop the ball and extract coordinates when the spinning ball is clicked
	document.addEventListener('click', function(e) {
		clearInterval(spinStart); // Stops the ball
	}, false);
	
}

spinBall(); // initial call to start spinning

/*
// Flies to target given
scene.camera.flyTo({
    destination : Cesium.Cartesian3.fromDegrees(-117.16, 32.71, 15000.0)
});
*/


